# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_08_10_234542) do

  create_table "activities", force: :cascade do |t|
    t.text "description"
    t.integer "lession_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["lession_id"], name: "index_activities_on_lession_id"
  end

  create_table "collaborations", force: :cascade do |t|
    t.integer "user_id"
    t.integer "course_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "kind"
    t.index ["course_id"], name: "index_collaborations_on_course_id"
    t.index ["user_id"], name: "index_collaborations_on_user_id"
  end

  create_table "courses", force: :cascade do |t|
    t.string "name"
    t.text "classPlan"
    t.string "enviromentSetting"
    t.integer "selective_process_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["selective_process_id"], name: "index_courses_on_selective_process_id"
  end

  create_table "feedbacks", force: :cascade do |t|
    t.text "content"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_feedbacks_on_user_id"
  end

  create_table "groups", force: :cascade do |t|
    t.integer "activity_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "submission_id"
    t.string "groupable_type"
    t.integer "groupable_id"
    t.index ["activity_id"], name: "index_groups_on_activity_id"
    t.index ["groupable_type", "groupable_id"], name: "index_groups_on_groupable_type_and_groupable_id"
    t.index ["submission_id"], name: "index_groups_on_submission_id"
  end

  create_table "groups_users", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "group_id"
    t.index ["group_id"], name: "index_groups_users_on_group_id"
    t.index ["user_id"], name: "index_groups_users_on_user_id"
  end

  create_table "lessions", force: :cascade do |t|
    t.integer "course_id"
    t.date "day"
    t.time "startTime"
    t.time "duration"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "feedback_id"
    t.index ["course_id"], name: "index_lessions_on_course_id"
    t.index ["feedback_id"], name: "index_lessions_on_feedback_id"
  end

  create_table "lessions_subjects", id: false, force: :cascade do |t|
    t.integer "lession_id", null: false
    t.integer "subject_id", null: false
    t.index ["lession_id", "subject_id"], name: "index_lessions_subjects_on_lession_id_and_subject_id"
    t.index ["subject_id", "lession_id"], name: "index_lessions_subjects_on_subject_id_and_lession_id"
  end

  create_table "materials", force: :cascade do |t|
    t.integer "course_id"
    t.date "day"
    t.text "description"
    t.string "link"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["course_id"], name: "index_materials_on_course_id"
  end

  create_table "participations", force: :cascade do |t|
    t.integer "user_id"
    t.integer "selective_process_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "situation"
    t.index ["selective_process_id"], name: "index_participations_on_selective_process_id"
    t.index ["user_id"], name: "index_participations_on_user_id"
  end

  create_table "presences", force: :cascade do |t|
    t.integer "user_id"
    t.integer "lession_id"
    t.integer "status"
    t.integer "performance"
    t.text "observation"
    t.time "minutes_late"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["lession_id"], name: "index_presences_on_lession_id"
    t.index ["user_id"], name: "index_presences_on_user_id"
  end

  create_table "selective_processes", force: :cascade do |t|
    t.integer "year"
    t.integer "schoolTerm"
    t.date "startDate"
    t.date "registrationDeadline"
    t.date "finishDate"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "subjects", force: :cascade do |t|
    t.string "subject"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "submissions", force: :cascade do |t|
    t.integer "status"
    t.string "link"
    t.integer "performance"
    t.text "observation"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "activity_id"
    t.integer "feedback_id"
    t.string "submisable_type"
    t.integer "submisable_id"
    t.index ["activity_id"], name: "index_submissions_on_activity_id"
    t.index ["feedback_id"], name: "index_submissions_on_feedback_id"
    t.index ["submisable_type", "submisable_id"], name: "index_submissions_on_submisable_type_and_submisable_id"
  end

  create_table "users", force: :cascade do |t|
    t.boolean "adm"
    t.string "name"
    t.string "email"
    t.string "password"
    t.integer "kind"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "submission_id"
    t.index ["submission_id"], name: "index_users_on_submission_id"
  end

end
