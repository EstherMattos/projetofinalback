class RemoveColumnFromFeedback < ActiveRecord::Migration[5.2]
  def change
    remove_column :feedbacks, :submission_id, :integer
    remove_column :feedbacks, :lession_id, :integer
    remove_column :submissions, :user_id, :integer
    remove_column :submissions, :group_id, :integer
  end
end
