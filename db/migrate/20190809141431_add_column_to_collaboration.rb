class AddColumnToCollaboration < ActiveRecord::Migration[5.2]
  def change
    add_column :collaborations, :kind, :integer
  end
end
