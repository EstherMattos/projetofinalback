class ChangingColumns < ActiveRecord::Migration[5.2]
  def change
    rename_column :activities, :lessions_id, :lession_id
    rename_column :lessions, :courses_id, :course_id
    rename_column :materials, :courses_id, :course_id
    rename_column :participations, :selective_processes_id, :selective_process_id

  end
end
