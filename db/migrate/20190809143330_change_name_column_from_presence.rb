class ChangeNameColumnFromPresence < ActiveRecord::Migration[5.2]
  def change
    rename_column :presences, :users_id, :user_id
    rename_column :presences, :lessions_id, :lession_id
    
  end
end
