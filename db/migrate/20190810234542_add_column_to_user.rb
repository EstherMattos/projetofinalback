class AddColumnToUser < ActiveRecord::Migration[5.2]
  def change
    add_reference :users, :submission, foreign_key: true
    add_reference :groups, :submission, foreign_key: true
    add_reference :groups, :groupable, polymorphic: true

    add_reference :lessions, :feedback, foreign_key: true
    add_reference :submissions, :feedback, foreign_key: true
    add_reference :submissions, :submisable, polymorphic: true
  end
end
