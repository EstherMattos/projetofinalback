class AddReferencesToSubmission < ActiveRecord::Migration[5.2]
  def change
    add_reference :submissions, :user, foreign_key: true
    add_reference :submissions, :group, foreign_key: true
  end
end
