class ChangeNameColumn < ActiveRecord::Migration[5.2]
  def change
    rename_column :courses, :selective_processes_id, :selective_process_id
  end
end
