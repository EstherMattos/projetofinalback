class AddActivityToSubmission < ActiveRecord::Migration[5.2]
  def change
    add_reference :submissions, :activity, foreign_key: true
  end
end
