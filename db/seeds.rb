# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
@usuario = User.create()

# MVP-1
PS =  SelectiveProcess.create()
    # MVP-5
    Participation.create(user_id: 1, selective_process_id: 1)

# MVP-2
Curso = Course.create(selective_process_id: 1)
MaterialDeApoio = Material.create(course_id: 1)

# MVP-3
AulaDeGit = Lession.create(course_id: 1)
Topico1 = Subject.create()
# Aula_Topico1 = LessionSubject.create(lession_id: 1, subject_id: 1)
PrimeiraAtividadeGit = Activity.create(lession_id: 1)

# MVP-4

# MVP-6
Presenca = Presence.create(user_id: 1, lession_id: 1)

# MVP-7
SubmissaoDeAtividade = Submission.create(activity_id: 1)

# MVP-8
Presentinho = Feedback.create(user_id: 1)

# MVP-9
Grupo1 = Group.create(activity_id: 1)
# UsuariosGrupo = UserGroup.create(user_id: 1, group_id: 1)
