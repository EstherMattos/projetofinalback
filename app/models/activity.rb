class Activity < ApplicationRecord
  belongs_to :lession, optional: true

  has_many :submissions
end
