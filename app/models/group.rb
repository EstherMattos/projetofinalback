class Group < ApplicationRecord
  belongs_to :activity

  has_many :users, as: :groupable

  has_many :users, through: :collaborations

end
