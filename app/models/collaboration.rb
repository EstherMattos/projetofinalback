class Collaboration < ApplicationRecord
  belongs_to :user
  belongs_to :course
  enum kind: [:instructor,  :monitor]
end
