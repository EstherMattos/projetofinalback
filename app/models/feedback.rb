class Feedback < ApplicationRecord
  has_many :lessions
  belongs_to :user
  has_many :submissions
end
