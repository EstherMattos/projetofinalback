class Submission < ApplicationRecord
  
  enum status: [:delivered, :undeliverable]
  enum performance: [:exemplary, :pattern, :bad, :terrible]
  has_many :users
  has_many :groups
  has_many :lessions, as: :submisable

end
