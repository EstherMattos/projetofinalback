class User < ApplicationRecord
    enum kind: [:member, :director, :exmember, :candidate, :externalCollaborator]

     has_many :participations
     has_many :selective_processes, through: :participations
     
     has_many :groups, as: :groupable

     has_many :feedbacks

     has_many :groups, through: :collaborations

end
