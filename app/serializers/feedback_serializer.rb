class FeedbackSerializer < ActiveModel::Serializer
  attributes :id, :content
  has_one :lession
  has_one :user
  has_one :submission
end
