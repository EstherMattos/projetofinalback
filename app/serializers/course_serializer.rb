class CourseSerializer < ActiveModel::Serializer
  attributes :id, :name, :classPlan, :enviromentSetting
  has_one :selective_process
end
