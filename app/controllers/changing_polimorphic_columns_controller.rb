class ChangingPolimorphicColumnsController < ApplicationController
  before_action :set_changing_polimorphic_column, only: [:show, :update, :destroy]

  # GET /changing_polimorphic_columns
  def index
    @changing_polimorphic_columns = ChangingPolimorphicColumn.all

    render json: @changing_polimorphic_columns
  end

  # GET /changing_polimorphic_columns/1
  def show
    render json: @changing_polimorphic_column
  end

  # POST /changing_polimorphic_columns
  def create
    @changing_polimorphic_column = ChangingPolimorphicColumn.new(changing_polimorphic_column_params)

    if @changing_polimorphic_column.save
      render json: @changing_polimorphic_column, status: :created, location: @changing_polimorphic_column
    else
      render json: @changing_polimorphic_column.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /changing_polimorphic_columns/1
  def update
    if @changing_polimorphic_column.update(changing_polimorphic_column_params)
      render json: @changing_polimorphic_column
    else
      render json: @changing_polimorphic_column.errors, status: :unprocessable_entity
    end
  end

  # DELETE /changing_polimorphic_columns/1
  def destroy
    @changing_polimorphic_column.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_changing_polimorphic_column
      @changing_polimorphic_column = ChangingPolimorphicColumn.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def changing_polimorphic_column_params
      params.fetch(:changing_polimorphic_column, {})
    end
end
