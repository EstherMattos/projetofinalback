require 'test_helper'

class ChangingPolimorphicColumnsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @changing_polimorphic_column = changing_polimorphic_columns(:one)
  end

  test "should get index" do
    get changing_polimorphic_columns_url, as: :json
    assert_response :success
  end

  test "should create changing_polimorphic_column" do
    assert_difference('ChangingPolimorphicColumn.count') do
      post changing_polimorphic_columns_url, params: { changing_polimorphic_column: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show changing_polimorphic_column" do
    get changing_polimorphic_column_url(@changing_polimorphic_column), as: :json
    assert_response :success
  end

  test "should update changing_polimorphic_column" do
    patch changing_polimorphic_column_url(@changing_polimorphic_column), params: { changing_polimorphic_column: {  } }, as: :json
    assert_response 200
  end

  test "should destroy changing_polimorphic_column" do
    assert_difference('ChangingPolimorphicColumn.count', -1) do
      delete changing_polimorphic_column_url(@changing_polimorphic_column), as: :json
    end

    assert_response 204
  end
end
